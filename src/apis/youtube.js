import axios from 'axios';

const KEY = 'AIzaSyCuZpjwrLOf479WhOYxtHvPHZq69ubKFXI';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});